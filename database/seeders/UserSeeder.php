<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'username' => 'admin',
                'password' => Hash::make('adminPassword'),
                'employee_id' => 1,
                'created_at' => now(),
            ],
            [
                'username' => 'kunikuzushi',
                'password' => Hash::make('employeePassword'),
                'employee_id' => 2,
                'created_at' => now(),
            ]
        ]);
    }
}
