<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudyMajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('study_majors')->insert([
            ['name' => 'Software Engineering', 'created_at' => now()],
            ['name' => 'Computer Science', 'created_at' => now()],
            ['name' => 'Information Systems', 'created_at' => now()],
            ['name' => 'Information Technology', 'created_at' => now()],
            ['name' => 'Computer Engineering', 'created_at' => now()],
            ['name' => 'Electrical Engineering', 'created_at' => now()],
            ['name' => 'Mechanical Engineering', 'created_at' => now()],
            ['name' => 'Civil Engineering', 'created_at' => now()],
            ['name' => 'Chemical Engineering', 'created_at' => now()],
            ['name' => 'Biomedical Engineering', 'created_at' => now()],
            ['name' => 'Aerospace Engineering', 'created_at' => now()],
            ['name' => 'Industrial Engineering', 'created_at' => now()],
            ['name' => 'Environmental Engineering', 'created_at' => now()],
            ['name' => 'Petroleum Engineering', 'created_at' => now()],
            ['name' => 'Nuclear Engineering', 'created_at' => now()],
            ['name' => 'Materials Engineering', 'created_at' => now()],
            ['name' => 'Agricultural Engineering', 'created_at' => now()],
            ['name' => 'Biotechnology Engineering', 'created_at' => now()],
            ['name' => 'Food Engineering', 'created_at' => now()],
            ['name' => 'Architectural Engineering', 'created_at' => now()],
            ['name' => 'Environmental Engineering', 'created_at' => now()],
            ['name' => 'Petroleum Engineering', 'created_at' => now()],
            ['name' => 'Nuclear Engineering', 'created_at' => now()],
            ['name' => 'Materials Engineering', 'created_at' => now()],
            ['name' => 'Agricultural Engineering', 'created_at' => now()],
            ['name' => 'Biotechnology Engineering', 'created_at' => now()],
            ['name' => 'Food Engineering', 'created_at' => now()],
            ['name' => 'Architectural Engineering', 'created_at' => now()],
            ['name' => 'Environmental Engineering', 'created_at' => now()],
            ['name' => 'Petroleum Engineering', 'created_at' => now()],
            ['name' => 'Nuclear Engineering', 'created_at' => now()]
        ]);
    }
}
