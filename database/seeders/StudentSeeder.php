<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('students')->insert([
            [
                'name' => 'Raiden Ei',
                'class_id' => 1,
                'nisn' => '0000011111',
                'nis' => '22230001',
                'address' => 'Sangonomiya Shrine, Inazuma',
                'phone_number' => '6281234567890',
                'created_at' => now(),
                'password' => Hash::make('password')
            ],
            [
                'name' => 'Raiden Shogun',
                'class_id' => 1,
                'nisn' => '0000011112',
                'nis' => '22230002',
                'address' => 'Grand Narukami Shrine, Inazuma',
                'phone_number' => '6281234567891',
                'created_at' => now(),
                'password' => Hash::make('password')
            ],
            [
                'name' => 'Raiden Kunikuzushi',
                'class_id' => 1,
                'nisn' => '0000011113',
                'nis' => '22230003',
                'address' => 'Grand Narukami Shrine, Inazuma',
                'phone_number' => '6281234567892',
                'created_at' => now(),
                'password' => Hash::make('password')
            ]
        ]);
    }
}
