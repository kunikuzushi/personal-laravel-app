<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudyMajorRequest;
use App\Models\StudyMajor;
use Illuminate\Http\Request;

class StudyMajorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $majors = StudyMajor::all();

        return view('pages.majors.index', [
            'majors' => $majors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.majors.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudyMajorRequest $request)
    {
        StudyMajor::create($request->validated());

        return redirect()->route('majors.index')->with('success', 'Major created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(StudyMajor $major)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(StudyMajor $major)
    {
        return view('pages.majors.edit', [
            'major' => $major
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreStudyMajorRequest $request, StudyMajor $major)
    {
        $major->update($request->validated());

        return redirect()->route('majors.index')->with('success', 'Major updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(StudyMajor $major)
    {
        $major->delete();

        return redirect()->route('majors.index')->with('success', 'Major deleted successfully');
    }
}
