<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClassRequest;
use App\Models\SchoolClass;
use App\Models\StudyMajor;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $classes = SchoolClass::all();

        return view('pages.classes.index', [
            'classes' => $classes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $studyMajors = StudyMajor::all('id', 'name');
        return view('pages.classes.create', [
            'studyMajors' => $studyMajors
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ClassRequest $request)
    {
        SchoolClass::create($request->validated());

        return redirect()->route('classes.index')->with('success', 'Class created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(SchoolClass $class)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SchoolClass $class)
    {
        $studyMajors = StudyMajor::all('id', 'name');
        return view('pages.classes.edit', [
            'class' => $class,
            'studyMajors' => $studyMajors
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ClassRequest $request, SchoolClass $class)
    {
        $class->update($request->validated());

        return redirect()->route('classes.index')->with('success', 'Class updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SchoolClass $class)
    {
        $class->delete();

        return redirect()->route('classes.index')->with('success', 'Class deleted successfully');
    }
}
