<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Tuition;
use Illuminate\Http\Request;

class TuitionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Tuition::all();
        $students = Student::all(['id', 'name']);
        return view('pages.tuitions.index', [
            'tuitions' => $data,
            'students' => $students
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.tuitions.create', [
            'students' => Student::all(['id', 'name'])
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric|min:1',
            'due_date' => 'required|date|after:today',
            'student_id' => 'required|numeric|min:1'
        ]);

        $student = Student::find($request->student_id);

        if (!$student) {
            return redirect()->back()->with('error', 'Student not found');
        }

        $tuition = new Tuition();
        $tuition->amount = $request->amount;
        $tuition->due_date = $request->due_date;
        $tuition->student_id = $request->student_id;
        $tuition->save();

        return redirect()->route('tuitions.index')->with('success', 'Tuition created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Tuition $tuition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tuition $tuition)
    {
        return view('pages.tuitions.edit', [
            'tuition' => $tuition,
            'students' => Student::all(['id', 'name'])
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Tuition $tuition)
    {
        $request->validate([
            'amount' => 'required|numeric|min:1',
            'due_date' => 'required|date|after:today',
            'student_id' => 'required|numeric|min:1'
        ]);

        $student = Student::find($request->student_id);

        if (!$student) {
            return redirect()->back()->with('error', 'Student not found');
        }

        $tuition->amount = $request->amount;
        $tuition->due_date = $request->due_date;
        $tuition->student_id = $request->student_id;
        $tuition->save();

        return redirect()->route('tuitions.index')->with('success', 'Tuition updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tuition $tuition)
    {
        $tuition->delete();
        return redirect()->route('tuitions.index')->with('success', 'Tuition deleted successfully');
    }
}
