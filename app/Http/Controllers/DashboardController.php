<?php

namespace App\Http\Controllers;

use App\Models\Tuition;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function employeeIndex()
    {
        return view('pages.dashboard.admin.index');
    }

    public function index()
    {
        return view('pages.dashboard.index', [
            'tuitions' => Tuition::whereStudentId(Auth::guard('students')->user()->id)
        ]);
    }
}
