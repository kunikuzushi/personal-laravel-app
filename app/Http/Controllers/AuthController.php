<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthEmployeeRequest;
use App\Http\Requests\AuthStudentRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginEmployee()
    {
        // if (Auth::guard('employee')->check()) {
        //     return redirect('/employee/dashboard');
        // }
        return view('pages.auth.employee.login');
    }


    public function authenticateEmployee(AuthEmployeeRequest $request)
    {
        if (Auth::guard('employee')->attempt($request->only('username', 'password'), $request->remember ?? false)) {
            return redirect('/employee/dashboard');
        }

        return back()->withErrors([
            'credentialsError' => 'The provided credentials do not match our records.',
        ]);
    }

    public function loginStudent()
    {
        // if (Auth::guard('students')->check()) {
        //     return redirect('/dashboard');
        // }
        return view('pages.auth.login');
    }

    public function authenticateStudent(AuthStudentRequest $request) {
        // dd($request->only('nis', 'password'));
        if (Auth::guard('students')->attempt($request->only('nis', 'password'), false)) {
            return redirect('/dashboard');
        }

        return back()->withErrors([
            'credentialsError' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout()
    {
        $isEmployee = Auth::guard('employee')->check();
        if ($isEmployee) {
            Auth::guard('employee')->logout();
            return redirect('/auth/employee/login');
        }

        Auth::guard('students')->logout();
        return redirect('/auth/login');
    }
}
