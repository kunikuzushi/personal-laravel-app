<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Models\Payment;
use App\Models\Tuition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Midtrans\Transaction;

class PaymentController extends Controller
{
    public function __construct()
    {
        \Midtrans\Config::$serverKey    = config('services.midtrans.serverKey');
        \Midtrans\Config::$isProduction = config('services.midtrans.isProduction');
        \Midtrans\Config::$isSanitized  = config('services.midtrans.isSanitized');
        \Midtrans\Config::$is3ds        = config('services.midtrans.is3ds');
    }

    public function acquireTransaction(Request $request, Tuition $tuition) {
        $existingPayment = Payment::where('tuition_id', $tuition->id)->first();
        if ($existingPayment !== null && $existingPayment->snap_token !== null) {
            if ($existingPayment->status === 'paid') {
                return response()->json(['message' => 'This invoice has been paid']);
            }

            return response()->json(['snap_token' => $existingPayment->snap_token]);
        }

        $snapToken = null;
        $transaction = Payment::create([
            'tuition_id' => $tuition->id,
            'status' => 'pending'
        ]);

        $student = $tuition->students()->first();

        $payload = [
            'transaction_details' => [
                'order_id' => $transaction->id,
                'gross_amount' => $tuition->amount
            ],
            'customer_details' => [
                'first_name' => explode(' ', $student->name)[0],
                'last_name' => implode(' ', array_slice(explode(' ', $student->name), 1)),
                'phone' => $student->phone_number
            ],
            'item_details' => [
                [
                    'id' => $tuition->id,
                    'price' => $tuition->amount,
                    'quantity' => 1,
                    'name' => 'Tuition Invoice @ '. date('d/m/Y', strtotime($tuition->due_date))
                ]
            ]
        ];

        $snapToken = \Midtrans\Snap::getSnapToken($payload);

        $transaction->snap_token = $snapToken;
        $transaction->save();

        return response()->json(['snap_token' => $snapToken]);
    }

    public function refreshTransaction(Request $request, Payment $payment) {
        $transaction = Transaction::status($payment->id);

        if ($transaction->transaction_status == 'settlement') {
            $payment->status = 'paid';
            $payment->save();
        }

        return response()->json(['status' => $payment->status]);
    }
}
