<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'class_id' => ['required', 'exists:classes,id', 'numeric'],
            'nis' => ['required', 'string', 'unique:students,nis', 'max:8', 'min:8'],
            'nisn' => ['required', 'string', 'unique:students,nisn', 'max:10', 'min:10'],
            'address' => ['required', 'string'],
            'phone_number' => ['required', 'string', 'max:20']
        ];

        if ($this->method() == 'PATCH' || $this->method() == 'PUT') {
            $rules['nis'] = ['required', 'string', 'max:8', 'min:8', Rule::unique('students', 'nis')->ignore($this->student->id)];
            $rules['nisn'] = ['required', 'string', 'max:10', 'min:10', Rule::unique('students', 'nisn')->ignore($this->student->id)];
        }

        return $rules;
    }
}
