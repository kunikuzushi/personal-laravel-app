<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tuition
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $amount
 * @property string $due_date
 * @property int $student_id
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tuition whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Student> $students
 * @property-read int|null $students_count
 * @property-read \App\Models\Payment|null $payment
 * @mixin \Eloquent
 */
class Tuition extends Model
{
    use HasFactory;
    protected $table = 'tuitions';

    public function students() {
        return $this->hasMany(Student::class, 'id', 'student_id');
    }

    public function payment() {
        return $this->hasOne(Payment::class, 'tuition_id', 'id');
    }
}
