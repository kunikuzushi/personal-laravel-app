<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StudyMajor
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|StudyMajor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyMajor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyMajor query()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyMajor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyMajor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyMajor whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyMajor whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\SchoolClass> $classes
 * @property-read int|null $classes_count
 * @mixin \Eloquent
 */
class StudyMajor extends Model
{
    use HasFactory;

    protected $table = 'study_majors';

    protected $fillable = [
        'name'
    ];

    public function classes() {
        return $this->hasMany(SchoolClass::class, 'study_major_id', 'id');
    }
}
