<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudyMajorController;
use App\Http\Controllers\TuitionController;
use App\Http\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/', 'pages.index')->middleware(['isStudent']);

Route::resource('tuitions', TuitionController::class)->middleware(['isEmployee']);
Route::resource('majors', StudyMajorController::class)->middleware(['isAdmin']);
Route::resource('classes', ClassController::class)->middleware(['isAdmin']);
Route::resource('students', StudentController::class)->middleware(['isAdmin']);

Route::controller(AuthController::class)->group(function () {
    Route::get('/auth/employee/login', 'loginEmployee')->name('auth.employee.login');
    Route::post('/auth/employee/login', 'authenticateEmployee')->name('auth.employee.authenticate');

    Route::get('/auth/login', 'loginStudent')->name('auth.student.login');
    Route::post('/auth/login', 'authenticateStudent')->name('auth.student.authenticate');

    Route::get('/auth/logout', 'logout')->name('auth.logout');
});

Route::controller(DashboardController::class)->group(function () {
    Route::get('/employee/dashboard', 'employeeIndex')->middleware(['isEmployee']);
    Route::get('/dashboard', 'index')->middleware(['isStudent'])->name('dashboard.student.index');
});

Route::post('/payments/{tuition}', [
    PaymentController::class,
    'acquireTransaction'
])->name('payments.acquire')->middleware(['isStudent']);

Route::post('/payments/{payment}/refresh', [
    PaymentController::class,
    'refreshTransaction'
])->name('payments.confirm')->middleware(['isStudent']);
