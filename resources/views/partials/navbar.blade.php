<x-container>
    <div class="navbar bg-base-100 p-0">
        <div class="navbar-start">
            <a class="text-xl font-bold">SchoolName</a>
        </div>
        @auth('employee')
        <div class="navbar-center hidden lg:flex">
            <ul class="menu menu-horizontal px-1">
                <li>
                    <details>
                        <summary>Tuitions</summary>
                        <ul class="p-2">
                            <li><a href="{{ route('tuitions.index') }}">List</a></li>
                            <li><a href="{{ route('tuitions.create') }}">New</a></li>
                        </ul>
                    </details>
                </li>
            </ul>
        </div>
        @endauth
        <div class="navbar-end gap-2">
            @php
                $user = Auth::guard('employee')->user() ?? Auth::guard('students')->user();
            @endphp
            @if($user !== null)
                @php
                    $name = $user instanceof App\Models\User ? $user->employees()->first()->name : $user->name;
                @endphp
                <div class="dropdown dropdown-end">
                    <div tabindex="0" role="button" class="btn btn-ghost btn-circle avatar">
                        <div class="w-10 rounded-full">
                            <img alt="Tailwind CSS Navbar component"
                                src="https://ui-avatars.com/api/?name={{ urlencode($name) }}" />
                        </div>
                    </div>
                    <ul tabindex="0"
                        class="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
                        <li>
                            <a class="justify-between">
                                Profile
                            </a>
                        </li>
                        <li><a>Settings</a></li>
                        <li><a href="{{ route('auth.logout') }}">Logout</a></li>
                    </ul>
                </div>
            @else
                <a href="{{ route('auth.student.login') }}" class="btn btn-primary btn-sm">Login</a>
            @endif
        </div>
    </div>
</x-container>
