<div class="container max-w-7xl px-3 lg:px-0 md:mx-auto {{ $class }}">
    {{ $slot }}
</div>