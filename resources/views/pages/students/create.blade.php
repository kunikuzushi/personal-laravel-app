@extends('layouts.app')

@section('title', 'Create Student')

@section('content')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            new SlimSelect({
                select: '#classIdSelect'
            });
        });
    </script>
    <x-container class="py-16">
        <h2 class="font-bold text-3xl">Creating Student</h2>
        <form method="post" class="w-full" action="{{ route('students.store') }}">
            @csrf
            @method('POST')
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">NIS</span>
                </div>
                <input name="nis" type="text" value="{{ old('nis') }}" placeholder="22231777"
                    class="input input-bordered input-md w-full @error('nis')
                        {{ 'input-error' }}
                    @enderror" />
                @error('nis')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">NISN</span>
                </div>
                <input name="nisn" type="text" value="{{ old('nisn') }}" placeholder="0000112233"
                    class="input input-bordered input-md w-full @error('nisn')
                        {{ 'input-error' }}
                    @enderror" />
                @error('nisn')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Student Name</span>
                </div>
                <input name="name" type="text" value="{{ old('name') }}" placeholder="Raiden Kunikuzushi"
                    class="input input-bordered input-md w-full @error('name')
                        {{ 'input-error' }}
                    @enderror" />
                @error('name')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <label class="form-control w-full h-28">
                <div class="label">
                    <span class="label-text">Address</span>
                </div>
                <textarea name="address" placeholder="Jl. Kembang Sepatu"
                    class="resize-none h-full textarea textarea-bordered input-md w-full @error('address')
                        {{ 'textarea-error' }}
                    @enderror">{{ old('address') }}</textarea>
                @error('address')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Phone Number</span>
                </div>
                <input name="phone_number" type="text" value="{{ old('phone_number') }}" placeholder="628123456789"
                    class="input input-bordered input-md w-full @error('phone_number')
                        {{ 'input-error' }}
                    @enderror" />
                @error('phone_number')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <label class="form-control w-full">
                <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Enrolled Class</span>
                </div>
                <select id="classIdSelect" name="class_id" class="py-3 rounded-lg px-3 w-full @error('class_id')
                {{ 'input-error' }} @enderror" id="select-beast" placeholder="Select the enrolled class" autocomplete="off">
                    <option value="" selected disabled>Select the enrolled class</option>
                    @foreach ($classes as $class)
                        <option value="{{ $class->id }}">
                            {{ $class->name }}
                        </option>
                    @endforeach
                </select>
                @error('class_id')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>

            <button type="submit" class="btn btn-primary w-full mt-4">Submit</button>
        </form>
    </x-container>
@endsection
