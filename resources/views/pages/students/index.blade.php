@extends('layouts.app')

@section('title', 'School Students')

@section('content')
    <x-container class="py-16 space-y-4">
        <h1 class="text-3xl font-bold">Manage Students</h1>
        @if ($message = Session::get('success'))
            <div id="alertSession" role="alert" class="alert alert-success py-1">
                <svg xmlns="http://www.w3.org/2000/svg" class="stroke-current shrink-0 h-6 w-6" fill="none"
                    viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                <span>{{ $message }}</span>
                <button class="btn btn-ghost rounded-full" onclick="document.getElementById('alertSession').remove()">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-x" width="16"
                        height="16" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none"
                        stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path d="M18 6l-12 12" />
                        <path d="M6 6l12 12" />
                    </svg></button>
            </div>
        @endif
        <div>
            <div class="w-full flex items-end">
                <a href="{{ route('students.create') }}" class="ml-auto btn btn-sm btn-primary"><svg xmlns="http://www.w3.org/2000/svg"
                        class="icon icon-tabler icon-tabler-plus" width="14" height="14" viewBox="0 0 24 24"
                        stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path d="M12 5l0 14" />
                        <path d="M5 12l14 0" />
                    </svg> New</a>
            </div>
            <div class="overflow-x-auto pb-8">
                <table class="table table-zebra">
                    <!-- head -->
                    <thead>
                        <tr class="text-center">
                            <th>No</th>
                            <th>NIS</th>
                            <th>NISN</th>
                            <th>Name</th>
                            <th>Enrolled Class</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $index = 0;
                        @endphp
                        @forelse ($students as $student)
                            <tr class="text-center">
                                <th>{{ $index + 1 }}</th>
                                <td>{{ $student->nis }}</td>
                                <td>{{ $student->nisn }}</td>
                                <td>{{ $student->name }}</td>
                                <td>{{ $student->studentClass()->first()->name }}</td>
                                <td>
                                    <form method="post" action="{{ route('students.destroy', $student->id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <a role="button" href="{{ route('students.show', $student->id) }}"
                                            class="btn btn-sm btn-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-eye" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0" />
                                                <path d="M21 12c-2.4 4 -5.4 6 -9 6c-3.6 0 -6.6 -2 -9 -6c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6" />
                                            </svg>
                                        </a>
                                        <a role="button" href="{{ route('students.edit', $student->id) }}"
                                            class="btn btn-sm btn-secondary">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit"
                                                width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5"
                                                stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                                <path
                                                    d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                                <path d="M16 5l3 3" />
                                            </svg>
                                        </a>
                                        <button type="submit" class="btn btn-sm btn-error">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-trash"
                                                width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5"
                                                stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                <path d="M4 7l16 0" />
                                                <path d="M10 11l0 6" />
                                                <path d="M14 11l0 6" />
                                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @php
                                $index++;
                            @endphp
                        @empty
                            <td colspan="6" class="text-center font-semibold text-lg py-4">No Data</td>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </x-container>
@endsection
