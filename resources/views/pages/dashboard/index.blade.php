@extends('layouts.app')

@section('title', 'Dashboard')

@php
    $student = Auth::guard('students')->user();
@endphp

@section('content')
    @if ($message = Session::get('snap_token'))
        @dd($message)
    @endif
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
    <script type="text/javascript">
        function refreshTransaction(orderId) {
            axios.post(`/payments/${orderId}/refresh`, {}, {
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    'Content-Type': 'application/json',
                }
            }).finally(() => {
                location.reload();
            });
        }
        function pay(id) {
            axios.post(`/payments/${id}`, {
                tuition_id: id
            }, {
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    'Content-Type': 'application/json',
                }
            }).then(response => {
                snap.pay(response.data.snap_token, {
                    onSuccess: function (result) {
                        refreshTransaction(result.order_id);
                    },
                    onPending: function (result) {
                        refreshTransaction(result.order_id);
                    },
                    onError: function (result) {
                        refreshTransaction(result.order_id);
                    }
                });
            });
        }
    </script>
    <x-container class="py-16 space-y-4">
        <h1 class="text-3xl font-bold">Welcome, {{ $student->name }}</h1>
        <p class="text-base">You have <b>{{ $tuitions->count() }}</b> tuitions</p>

        <div class="overflow-x-auto pb-8">
            <table class="table table-zebra">
                <!-- head -->
                <thead class="text-center">
                    <tr>
                        <th>No</th>
                        <th>Amount</th>
                        <th>Invoiced At</th>
                        <th>Due Date</th>
                        <th>Payment Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @php
                        $index = 0;
                    @endphp
                    @forelse ($tuitions->get() as $tuition)
                        <tr>
                            <th>{{ $index + 1 }}</th>
                            <td>Rp {{ number_format($tuition->amount, 2, ',', '.') }}</td>
                            <td>{{ date('d/m/Y', strtotime($tuition->created_at)) }}</td>
                            <td>{{ date('d/m/Y', strtotime($tuition->due_date)) }}</td>
                            @php
                                $payment = $tuition->payment()->first();
                            @endphp
                            @if ($payment !== null)
                                <td>
                                    <span class="badge badge-success">{{ $payment->status }}</span>
                                </td>
                            @else
                                <td>
                                    <span class="badge badge-error">unpaid</span>
                                </td>
                            @endif
                            <td>
                                <button class="btn btn-sm btn-primary pay-button {{ $payment && $payment->status == 'paid' ? 'btn-disabled cursor-not-allowed' : '' }}" onclick="pay({{ $tuition->id }})" {{ $payment && $payment->status == 'paid' ? 'disabled' : '' }}>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-receipt" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M5 21v-16a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v16l-3 -2l-2 2l-2 -2l-2 2l-2 -2l-3 2m4 -14h6m-6 4h6m-2 4h2" />
                                    </svg>
                                </button>
                            </td>
                        </tr>
                        @php
                            $index++;
                        @endphp
                    @empty
                        <td colspan="6" class="text-center font-semibold text-lg py-4">No Data</td>
                    @endforelse
                </tbody>
            </table>
        </div>
    </x-container>
@endsection
