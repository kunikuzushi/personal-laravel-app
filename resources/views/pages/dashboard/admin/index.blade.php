@extends('layouts.app')

@section('title', 'Dashboard')

@php
    $employee = Auth::guard('employee')->user()->employees()->first();
@endphp

@section('content')
    <x-container class="py-16 space-y-4">
        <h1 class="text-3xl font-bold">Welcome, {{ $employee->name }}</h1>
        <p class="text-base">You are logged in as {{ $employee->role }}.</p>
    </x-container>
@endsection
