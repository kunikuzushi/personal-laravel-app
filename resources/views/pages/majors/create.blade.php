@extends('layouts.app')

@section('title', 'Create Study Major')

@section('content')
    <x-container class="py-16">
        <h2 class="font-bold text-3xl">Creating Study Major</h2>
        <form method="post" class="w-full" action="{{ route('majors.store') }}">
            @csrf
            @method('POST')
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Study Major Name</span>
                </div>
                <input name="name" type="text" required value="{{ old('name') }}" placeholder="Software Engineering"
                    class="input input-bordered input-md w-full @error('name')
                        {{ 'input-error' }}
                    @enderror" />
                @error('name')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <button type="submit" class="btn btn-primary w-full mt-4">Submit</button>
        </form>
    </x-container>
@endsection
