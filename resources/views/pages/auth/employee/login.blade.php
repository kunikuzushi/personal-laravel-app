@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <x-container class="py-16 space-y-4 flex items-center">
        <section class="max-w-md w-full mx-auto space-y-4">
            <h1 class="text-3xl font-bold">Login</h1>

            @if ($message = $errors->first('credentialsError'))
                <div id="alertSession" role="alert" class="alert alert-error py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-alert-circle-filled"
                        width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50"
                        fill="none" stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M12 2c5.523 0 10 4.477 10 10a10 10 0 0 1 -19.995 .324l-.005 -.324l.004 -.28c.148 -5.393 4.566 -9.72 9.996 -9.72zm.01 13l-.127 .007a1 1 0 0 0 0 1.986l.117 .007l.127 -.007a1 1 0 0 0 0 -1.986l-.117 -.007zm-.01 -8a1 1 0 0 0 -.993 .883l-.007 .117v4l.007 .117a1 1 0 0 0 1.986 0l.007 -.117v-4l-.007 -.117a1 1 0 0 0 -.993 -.883z"
                            stroke-width="0" fill="currentColor" />
                    </svg>
                    <span class="text-sm">{{ $message }}</span>
                    <button class="btn btn-ghost rounded-full" onclick="document.getElementById('alertSession').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-x" width="16"
                            height="16" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path d="M18 6l-12 12" />
                            <path d="M6 6l12 12" />
                        </svg></button>
                </div>
            @endif

            <form method="POST" action="{{ route('auth.employee.authenticate') }}" class="space-y-2">
                @csrf
                @method('POST')

                <div class="form-control w-full">
                    <label
                        class="input input-bordered flex items-center gap-2 @error('username')
                        {{ 'input-error' }}
                    @enderror">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor"
                            class="w-4 h-4 opacity-70">
                            <path
                                d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6ZM12.735 14c.618 0 1.093-.561.872-1.139a6.002 6.002 0 0 0-11.215 0c-.22.578.254 1.139.872 1.139h9.47Z" />
                        </svg>
                        <input name="username" value="{{ old('username') }}" type="text" class="grow"
                            placeholder="Username" />
                    </label>
                    @error('username')
                        <div class="label">
                            <span class="label-text-alt text-red-300">{{ $message }}</span>
                        </div>
                    @enderror
                </div>


                <div class="form-control w-full">
                    <label
                        class="input input-bordered flex items-center gap-2 @error('password')
                        {{ 'input-error' }}
                    @enderror">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor"
                            class="w-4 h-4 opacity-70">
                            <path fill-rule="evenodd"
                                d="M14 6a4 4 0 0 1-4.899 3.899l-1.955 1.955a.5.5 0 0 1-.353.146H5v1.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-2.293a.5.5 0 0 1 .146-.353l3.955-3.955A4 4 0 1 1 14 6Zm-4-2a.75.75 0 0 0 0 1.5.5.5 0 0 1 .5.5.75.75 0 0 0 1.5 0 2 2 0 0 0-2-2Z"
                                clip-rule="evenodd" />
                        </svg>
                        <input name="password" type="password" class="grow" placeholder="Password" />
                    </label>
                    @error('password')
                        <div class="label">
                            <span class="label-text-alt text-red-300">{{ $message }}</span>
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary w-full">Submit</button>
            </form>

        </section>
    </x-container>
@endsection
