@extends('layouts.app')

@section('title', 'Create Tuition')

@section('content')
    <x-container class="py-16">
        <h2 class="font-bold text-3xl">Creating Tuition</h2>
        <form method="post" class="w-full" action="{{ route('tuitions.store') }}">
            @csrf
            @method('POST')
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Student</span>
                </div>
                <select name="student_id" class="select select-bordered w-full @error('student_id')
                        {{ 'input-error' }}
                    @enderror">
                    <option disabled selected>Select student</option>
                    @foreach ($students as $s)
                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                    @endforeach
                </select>
                @error('student_id')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Amount</span>
                </div>
                <input name="amount" min="1" type="number" value="{{ old('amount') }}" placeholder="100000"
                    class="input input-bordered input-md w-full @error('amount')
                        {{ 'input-error' }}
                    @enderror" />
                @error('amount')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Due Date</span>
                </div>
                <input name="due_date" type="date" value="{{ old('due_date') }}"
                    class="input input-bordered input-md w-full @error('amount')
                        {{ 'input-error' }}
                    @enderror" />
                @error('due_date')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>

            <button type="submit" class="btn btn-primary w-full mt-4">Submit</button>
        </form>
    </x-container>
@endsection
