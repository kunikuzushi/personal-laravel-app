@extends('layouts.app')

@section('title', 'Create School Class')

@section('content')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            new SlimSelect({
                select: '#majorIdSelect'
            });
        });
    </script>
    <x-container class="py-16">
        <h2 class="font-bold text-3xl">Creating New Class</h2>
        <form method="post" class="w-full" action="{{ route('classes.store') }}">
            @csrf
            @method('POST')
            <label class="form-control w-full">
                <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Class Name</span>
                </div>
                <input name="name" type="text" required value="{{ old('name') }}" placeholder="XI RPL 2"
                    class="input input-bordered input-md w-full @error('name')
                        {{ 'input-error' }}
                    @enderror" />
                @error('name')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>

            <label class="form-control w-full">
                <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Study Major</span>
                </div>
                <select id="majorIdSelect" name="study_major_id" class="py-3 rounded-lg px-3 w-full @error('study_major_id')
                {{ 'input-error' }} @enderror" id="select-beast" placeholder="Select a study major" autocomplete="off">
                    <option value="" selected disabled>Select a study major</option>
                    @foreach ($studyMajors as $studyMajor)
                        <option value="{{ $studyMajor->id }}">
                            {{ $studyMajor->name }}
                        </option>
                    @endforeach
                </select>
                @error('study_major_id')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>

            <button type="submit" class="btn btn-primary w-full mt-4">Create</button>
        </form>
    </x-container>
@endsection
