@extends('layouts.app')

@section('title', 'Edit School Class')

@section('content')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            new SlimSelect({
                select: '#majorIdSelect'
            });
        });
    </script>
    <x-container class="py-16">
        <h2 class="font-bold text-3xl">Updating {{ $class->name }}</h2>
        <form method="post" class="w-full" action="{{ route('classes.update', $class->id) }}">
            @csrf
            @method('PATCH')
            <label class="form-control w-full">
                <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Class Name</span>
                </div>
                <input name="name" type="text" required value="{{ old('name') ?? $class->name }}" placeholder="XI RPL 2"
                    class="input input-bordered input-md w-full @error('name')
                        {{ 'input-error' }}
                    @enderror" />
                @error('name')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>

            <label class="form-control w-full">
                <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Study Major</span>
                </div>
                <select id="majorIdSelect" name="study_major_id" class="py-3 rounded-lg px-3 w-full @error('study_major_id')
                {{ 'input-error' }} @enderror" id="select-beast" placeholder="Select a study major" autocomplete="off">
                    @foreach ($studyMajors as $studyMajor)
                        <option value="{{ $studyMajor->id }}" {{ $class->study_major_id == $studyMajor->id ? 'selected' : '' }}>
                            {{ $studyMajor->name }}
                        </option>
                    @endforeach
                </select>
                @error('study_major_id')
                    <div class="label">
                        <span class="label-text-alt text-red-300">{{ $message }}</span>
                    </div>
                @enderror
            </label>

            <button type="submit" class="btn btn-primary w-full mt-4">Update</button>
        </form>
    </x-container>
@endsection
