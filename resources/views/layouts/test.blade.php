@section('content')
<div class="flex flex-col items-center justify-center gap-y-4">
    <h1 class="text-center font-bold text-4xl">@yield('content-title')</h1>
    <div class="grid grid-cols-3 gap-x-4">
        <a class="btn" href="/">Home</a>
        <a class="btn" href="/about">About</a>
        <a class="btn" href="/contact">Contact</a>
    </div>
</div>
@endsection
